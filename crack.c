#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings


// Stucture to hold both a plaintext password and a hash.
struct entry 
{
    // TODO: FILL THIS IN
    char word[PASS_LEN];
	char hash[HASH_LEN];
};

int by_hash(const void *a, const void *b)
{
    struct entry *aa = (struct entry *)a;
    struct entry *bb = (struct entry *)b;
    return strcmp(aa->hash, bb->hash);
}

int search_by_hash(const void *t, const void *e)
{
    return strcmp((char *)t, ((struct entry *)e)->hash);
}


// TODO
// Read in the dictionary file and return an array of structs.
// Each entry should contain both the hash and the dictionary
// word.
struct entry *read_dictionary(char *filename, int *size)
{
    *size = 0;
    int lines = 50;
    struct entry * entries = malloc(lines * sizeof(struct entry));
    FILE *d = fopen(filename, "r");
    char line[1000];
    int count = 0;
    while (fgets(line, 1000, d) != NULL)
    {
        if (count == lines)
        {
            lines += 50;
            entries = realloc(entries, lines * sizeof(struct entry));
        }
        
        line[strlen(line) - 1] = '\0';
        strcpy(entries[count].word, line);
        
        char *h = md5(entries[count].word, strlen(entries[count].word));
        strcpy(entries[count].hash, h);
        
        free(h);
        count++;
    }
    
    *size = count;
    fclose(d);
    return entries;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the dictionary file into an array of entry structures
    int dlen = 0;
    struct entry *dict = read_dictionary(argv[2], &dlen);
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    qsort(dict, dlen, sizeof(struct entry), by_hash);

    // TODO
    // Open the hash file for reading.
    FILE *h = fopen(argv[1], "r");
    

    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary word.
    // Print out both the hash and word.
    // Need only one loop. (Yay!)
    char line[HASH_LEN];
    while (fgets(line, HASH_LEN, h) != NULL)
    {
        line[HASH_LEN - 1] = '\0';
        
        for (int i = 0; i < dlen; i++)
        {
            struct entry * found = bsearch(line, dict, dlen, sizeof(struct entry), search_by_hash);
            
            if (found)
            {
                printf("%s : %s\n", found->hash, found->word);
                break;
            }
        }
    }
    
    fclose(h);
    free(dict);
}
